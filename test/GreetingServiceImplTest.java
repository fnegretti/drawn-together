import static org.junit.Assert.*;


import java.util.Date;
import org.junit.After;
import org.junit.Test;
import com.doodleproject.dt.server.GreetingServiceImpl;
import com.doodleproject.dt.shared.Commento;
import com.doodleproject.dt.shared.Event;
import com.doodleproject.dt.shared.Partecipation;
import com.doodleproject.dt.shared.User;
public class GreetingServiceImplTest {
	
	User ciro = new User("ciro","sceik101","ciao","ciro@ciro","1423423");
	User copiauser = new User("ciccio","sceik101","ciaociao","ciro@ciro","rqwerwer");
	User davide = new User("Davide", "veleno", "ciao", "pi@pi","fadsl�f");
	Event e = new Event("Social","Bologna","Spaccatiammerda","sceik101","novembre","dicembre");
	Partecipation p = new Partecipation(0, "veleno");
	Commento c = new Commento("ciao stronzo",0,"veleno");
	
	@After
	public final void clearDB(){
		GreetingServiceImpl server = new GreetingServiceImpl();
		server.cancellaDBContent();
	}

	@Test
	public final void testRegistration() {
		GreetingServiceImpl server = new GreetingServiceImpl();
		assertEquals(ciro.getRandomCookieValue(), server.registration(ciro));
		assertEquals(null, server.registration(copiauser));
		assertEquals(davide.getRandomCookieValue(), server.registration(davide));
	
	}

	@Test
	public final void testLogin() {
		
		GreetingServiceImpl server = new GreetingServiceImpl();
		System.out.println("test login registration");
		assertEquals(ciro.getRandomCookieValue(), server.registration(ciro));
		assertEquals(null, server.login(copiauser));
		assertEquals(null, server.login(davide));
		System.out.println("ciro pwd:"+ciro.getPassword());
		assertEquals(ciro.getRandomCookieValue(), server.login(ciro));
	}
	
	@Test
	public final void testInsertEvent() {
		GreetingServiceImpl server = new GreetingServiceImpl();
		server.openDB();
		assertTrue(server.db.getTreeMap("event").isEmpty());
		server.db.close();
		server.insertEvent(e);
		server.openDB();
		assertFalse(server.db.getTreeMap("event").isEmpty());
		server.db.close();
	}
	@Test
	public final void testInsertPartecipation() {
		GreetingServiceImpl server = new GreetingServiceImpl();
		server.openDB();
		assertTrue(server.db.getTreeMap("sondaggio").isEmpty());
		server.db.close();
		server.insertPartecipation(p);
		server.openDB();
		assertFalse(server.db.getTreeMap("sondaggio").isEmpty());
		server.db.close();
	}
	@Test
	public final void testInsertComment() {
		GreetingServiceImpl server = new GreetingServiceImpl();
		server.openDB();
		assertTrue(server.db.getTreeMap("commenti").isEmpty());
		server.db.close();
		server.insertComment(c);
		server.openDB();
		assertFalse(server.db.getTreeMap("commenti").isEmpty());
		server.db.close();
	}
	@Test
	public final void testDeleteCookie() {
		GreetingServiceImpl server = new GreetingServiceImpl();
		assertFalse(server.deleteCookie(ciro.getRandomCookieValue()));
		assertEquals(ciro.getRandomCookieValue(), server.registration(ciro));
		assertTrue(server.deleteCookie(ciro.getRandomCookieValue()));
	}
	@Test
	public final void testChiudiEventi() {
		long d = new Date().getTime();
		int [] vect = new int [1];
		e.setStartDatenumber(d+1);
		e.setEndDatenumber(d+2);
		e.setClose(false);
		GreetingServiceImpl server = new GreetingServiceImpl();
		server.insertEvent(e);
		vect[0]=e.getID();
		server.chiudiEventi(vect);
		server.openDB();
		assertTrue(((Event) server.db.getTreeMap("event").get(e.getID())).getClose());
		server.db.close();
		
	}
	@Test
	public final void testLimitPartecipation() {
		
		GreetingServiceImpl server = new GreetingServiceImpl();
		assertEquals(ciro.getRandomCookieValue(), server.registration(ciro));
			server.insertEvent(e);
			assertTrue(server.limitPartecipation("veleno",e.getID()));
			assertTrue(server.insertPartecipation(p));
			assertFalse(server.limitPartecipation("veleno",e.getID()));
	}

	@Test
	public final void testAllCommentForAnEvent() {
	
		GreetingServiceImpl server = new GreetingServiceImpl();
		assertEquals(ciro.getRandomCookieValue(), server.registration(ciro));
		server.insertEvent(e);
		assertNull(server.allCommentForAnEvent(e.getID()));
		server.insertComment(c);
		assertNotNull(server.allCommentForAnEvent(e.getID()));
	}

	@Test
	public final void testAllEvents() {
	
		GreetingServiceImpl server = new GreetingServiceImpl();
		assertEquals(ciro.getRandomCookieValue(), server.registration(ciro));
		assertNull(server.allEvents());
		server.insertEvent(e);
		assertNotNull(server.allEvents());

	}

	@Test
	public final void testCloseEvent() {

		GreetingServiceImpl server = new GreetingServiceImpl();
		assertEquals(ciro.getRandomCookieValue(), server.registration(ciro));
		server.insertEvent(e);
		assertFalse(server.closeEvent(1));
		assertTrue(server.closeEvent(e.getID()));
		assertFalse(server.closeEvent(e.getID()));
	}

	@Test
	public final void testCancelEvent() {
	
		GreetingServiceImpl server = new GreetingServiceImpl();
			assertEquals(ciro.getRandomCookieValue(), server.registration(ciro));
			assertFalse(server.cancelEvent(e.getID()));
			server.insertEvent(e);
			assertTrue(server.insertPartecipation(p));
			server.insertComment(c);
			assertTrue(server.cancelEvent(e.getID()));
			assertNull(server.allCommentForAnEvent(e.getID()));
			assertNull(server.allEvents());
			
	}

	@Test
	public final void testAllEventsUser() {
		
		GreetingServiceImpl server = new GreetingServiceImpl();
		assertEquals(ciro.getRandomCookieValue(), server.registration(ciro));
		assertNull(server.allEventsUser(ciro.getUsername()));
		server.insertEvent(e);
		assertNotNull(server.allEventsUser(ciro.getUsername()));
	
	}

	@Test
	public final void testAllEventsOfOtherUsers() {
		
		GreetingServiceImpl server = new GreetingServiceImpl();
		assertEquals(ciro.getRandomCookieValue(), server.registration(ciro));
		assertNull(server.allEventsOfOtherUsers(davide.getUsername()));
		server.insertEvent(e);
		assertNotNull(server.allEventsOfOtherUsers(davide.getUsername()));
	}

	@Test
	public final void testThereIsANewEvent() {
		
		GreetingServiceImpl server = new GreetingServiceImpl();
		
		assertNull(server.thereIsANewEvent(1,"anonimus"));
		assertNull(server.thereIsANewEvent(2,ciro.getUsername()));
		assertNull(server.thereIsANewEvent(3,davide.getUsername()));
		assertEquals(ciro.getRandomCookieValue(), server.registration(ciro));
		server.insertEvent(e);
		assertNotNull(server.thereIsANewEvent(1, "anonimus"));
		assertNotNull(server.thereIsANewEvent(2,ciro.getUsername()));
		assertNotNull(server.thereIsANewEvent(3,davide.getUsername()));;
		

	}

	@Test
	public final void testDeletePartecipation() {
	
		GreetingServiceImpl server = new GreetingServiceImpl();
		assertEquals(ciro.getRandomCookieValue(), server.registration(ciro));
		server.insertEvent(e);
		assertTrue(server.limitPartecipation(p.getUsername(), p.getEventId()));
		assertTrue(server.insertPartecipation(p));
		assertFalse(server.limitPartecipation(p.getUsername(), p.getEventId()));
		assertTrue(server.deletePartecipation(p));
		assertTrue(server.limitPartecipation(p.getUsername(), p.getEventId()));

	}

	@Test
	public final void testControllCookiesinDB() {
		
		GreetingServiceImpl server = new GreetingServiceImpl();
		assertFalse(server.controllCookiesinDB(ciro.getUsername(),ciro.getRandomCookieValue()));		
		assertEquals(ciro.getRandomCookieValue(), server.registration(ciro));
		assertTrue(server.controllCookiesinDB(ciro.getUsername(),ciro.getRandomCookieValue()));
		
	}

}
