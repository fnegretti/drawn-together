package com.doodleproject.dt.client;

import java.util.Date;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ValueBoxBase.TextAlignment;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DatePicker;
//sezione dedicata alla creazione di nuovi eventi
public class NewEventWindowLogged extends VerticalPanel{
	Drawn_Together_2_0 indexPage;
	HorizontalPanel startDatePanel,endDatePanel,finalPanelH;
	VerticalPanel textVPanel,dateVPanel,endPanel;
	TextBox eventName, location, startEventHour, endEventHour;
	DatePicker startEventDate, endEventDate;
	TextArea description;
	Label startEventLabel, endEventLabel;
	Date startDate,endDate;
	Button createEventButton;
	GreetingServiceAsync RPC;
	String admin;
	Long start;
	Long end;
	ClickHandler unClick;
	int conta=0;

	public NewEventWindowLogged(GreetingServiceAsync RPC, String admin,Drawn_Together_2_0 indexPage ) {
		this.RPC = RPC;
		this.admin = admin;
		initAllVariables();
	}

	public void initAllVariables(){
		//crea tutti gli oggetti del pannello
		eventName = new TextBox();
		location = new TextBox();
		description = new TextArea();
		startEventHour = new TextBox();
		endEventHour = new TextBox();
		startEventDate = new DatePicker();
		endEventDate = new DatePicker();
		startEventLabel = new Label();
		endEventLabel 	= new Label();
		createEventButton = new Button("Create");
		startDatePanel = new HorizontalPanel();
		endDatePanel = new HorizontalPanel();
		textVPanel = new VerticalPanel();
		dateVPanel = new VerticalPanel();
		endPanel = new VerticalPanel();
		finalPanelH = new HorizontalPanel();
		endDate = new Date();

		eventName.getElement().setPropertyString("placeholder", "Name:");
		location.getElement().setPropertyString("placeholder", "Location:");
		description.getElement().setPropertyString("placeholder", "Descrizione:");
		startEventHour.getElement().setPropertyString("placeholder", "00:00");
		endEventHour.getElement().setPropertyString("placeholder", "00:00");
		//pannelli data
		startDatePanel.add(startEventHour);
		startDatePanel.add(startEventLabel);
		endDatePanel.add(endEventHour);
		endDatePanel.add(endEventLabel);
		//TEXT VARI
		startEventHour.setSize("40px", "12px");
		endEventHour.setSize("40px", "12px");

		startEventDate.addValueChangeHandler(new ValueChangeHandler<Date>() {
			public void onValueChange(ValueChangeEvent<Date> event) {
				Date date = correggiDatePicker(event.getValue());
				long currentDate = new Date().getTime();
				long dayMillSec = 1000*60*60*24;
				long startDated = date.getTime();
				start = startDated;
				DateTimeFormat d= DateTimeFormat.getFormat("dd-MM-yyyy");
				if((currentDate-dayMillSec)> (startDated))
					Window.alert("data di inizio evento precedente alla data attuale");
				else startEventLabel.setText(d.format(date)); 
			}
		});

		//DATEPICKER che verr� usato per settare la fine di un evento
		endEventDate.addValueChangeHandler(new ValueChangeHandler<Date>() {
			public void onValueChange(ValueChangeEvent<Date> event) {
				Date date = correggiDatePicker(event.getValue());
				long currentDate = new Date().getTime();
				long dayMillSec = 1000*60*60*24;
				long endDated = date.getTime();
				end = endDated;
				DateTimeFormat d= DateTimeFormat.getFormat("dd-MM-yyyy");
				if((currentDate-dayMillSec)> (endDated))
					Window.alert("data di fine evento precedente alla data attuale");
				else endEventLabel.setText(d.format(date)); 
			}
		});
		//ID PER CSS
		createEventButton.getElement().setId("createEvent");
	}
	//pannello finestra dati evento
	public VerticalPanel createWindow(){
		textVPanel.add(eventName);
		textVPanel.add(location);
		textVPanel.add(description);
		textVPanel.add(createEventButton);
		dateVPanel.add(startEventDate);
		dateVPanel.add(startDatePanel);
		dateVPanel.add(endEventDate);
		dateVPanel.add(endDatePanel);
		finalPanelH.add(textVPanel);
		finalPanelH.add(dateVPanel);
		endPanel.add(finalPanelH);
		createEventButton.setSize("210px", "30px");
		eventName.setWidth("210px");
		location.setWidth("210px");
		description.setSize("210px","341px");
		description.setAlignment(TextAlignment.JUSTIFY);
		startDatePanel.setSpacing(5);
		endDatePanel.setSpacing(5);
		textVPanel.setSpacing(4);
		dateVPanel.setSpacing(5);
		return endPanel;
	}
	//serve per settare sempre l'inizio (perr defaul) alle 00:00 anche se gli orologi di browser o pc sono sballati
	private Date correggiDatePicker(Date d){
		String Hour,Min,Sec;
		long dateNum = d.getTime();
		long errore;
		long sec = 1000;
		long min = 1000*60;
		long hour = 1000*60*60;
		Hour =d.toString().substring(11,13);
		Min = d.toString().substring(14, 16);
		Sec = d.toString().substring(17, 19);
		errore= sec*Integer.parseInt(Sec)+min*Integer.parseInt(Min)+hour*Integer.parseInt(Hour);
		d.setTime((dateNum-errore));
		return d;
	}
}