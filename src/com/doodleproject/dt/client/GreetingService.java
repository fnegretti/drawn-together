package com.doodleproject.dt.client;

import com.doodleproject.dt.shared.Commento;
import com.doodleproject.dt.shared.ListComment;
import com.doodleproject.dt.shared.ListEvent;
import com.doodleproject.dt.shared.Partecipation;
import com.doodleproject.dt.shared.User;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.doodleproject.dt.shared.Event;
/**
 * Interfaccia client-side delle chiamate RPC
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {
	String registration(User user) throws IllegalArgumentException;
	String login(User user) throws Exception;
	void insertEvent(Event e);
	boolean insertPartecipation (Partecipation p);
	boolean limitPartecipation (String username, int idEvento);
	void insertComment(Commento e);
	ListComment allCommentForAnEvent(int idEvent);
	ListEvent allEvents();
	boolean closeEvent(int idEvent);
	boolean cancelEvent(int idEvent);
	ListEvent allEventsUser(String user);
	ListEvent allEventsOfOtherUsers(String user);
	ListEvent thereIsANewEvent(int typeOfSearch, String admin);
	boolean deletePartecipation (Partecipation p);
	public boolean controllCookiesinDB(String session,String value);
	public boolean deleteCookie(String nameCookie);
	void chiudiEventi(int [] vect);

}