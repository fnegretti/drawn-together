package com.doodleproject.dt.client;

import com.doodleproject.dt.shared.Commento;
import com.doodleproject.dt.shared.Event;
import com.doodleproject.dt.shared.ListComment;
import com.doodleproject.dt.shared.ListEvent;
import com.doodleproject.dt.shared.Partecipation;
import com.doodleproject.dt.shared.User;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Controparte asincrona di GreetingService
 */
public interface GreetingServiceAsync {
	void registration(User user, AsyncCallback<String> callback)
			throws IllegalArgumentException;
	void login(User user, AsyncCallback<String> callback)
			throws Exception;
	void insertEvent(Event e, AsyncCallback<Void> callback);
	void insertPartecipation(Partecipation p, AsyncCallback<Boolean> callback);
	void limitPartecipation(String username, int idEvento,
			AsyncCallback<Boolean> callback);
	void insertComment(Commento e, AsyncCallback<Void> callback);
	void allCommentForAnEvent(int idEvent, AsyncCallback<ListComment> callback);
	void allEvents(AsyncCallback<ListEvent> callback);
	void closeEvent(int idEvent, AsyncCallback<Boolean> callback);
	void cancelEvent(int idEvent, AsyncCallback<Boolean> callback);
	void allEventsUser(String user, AsyncCallback<ListEvent> callback);
	void allEventsOfOtherUsers(String user, AsyncCallback<ListEvent> callback);
	void thereIsANewEvent(int typeOfSearch, String admin, AsyncCallback<ListEvent> callback);
	void deletePartecipation(Partecipation p, AsyncCallback<Boolean> callback);
	void controllCookiesinDB(String session,String value, AsyncCallback<Boolean> callback);
	void chiudiEventi(int[] vect, AsyncCallback<Void> callback);
	void deleteCookie(String nameCookie, AsyncCallback<Boolean> callback);

}
