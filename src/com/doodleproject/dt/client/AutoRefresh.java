package com.doodleproject.dt.client;

import java.util.Collection;
import java.util.Date;

import com.doodleproject.dt.shared.Event;
import com.doodleproject.dt.shared.ListEvent;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RecordList;
// timer che ogni 60 sec fa il refresh delle tabelle e chiude gli eventi scaduti
public class AutoRefresh extends Timer {
	GreetingServiceAsync c;
	WindowPartecipation a;
	Grid griglia;
	char typeGrid;
	int cooldown = 60000;

	public  AutoRefresh(GreetingServiceAsync callBack, WindowPartecipation a, char typeGrid ){
		c = callBack;
		this.a = a;
		this.typeGrid = typeGrid;
		
	}

	public AutoRefresh(Grid g, char typeGrid){
		griglia = g;
		this.typeGrid = typeGrid;
	}

	public void run() {
		// a seconda del parametro viene eseguito il metodo corrispondente	
		switch(typeGrid){
		case 'W': //commenti
			a.commentFormat(c);
			this.schedule(cooldown);
			break;
		case 'A'://tutti gli eventi
			refreshAnonimus();
			break;
		case 'L'://propri eventi
			refreshUserLogged();
			break;
		case 'O'://eventi di altri
			refreshOtherUsers();
			break;
		default: 
			break;
		}
	}

	public void refreshUserLogged(){
		//recupera username dai cookies
		Collection<String> allcookies = Cookies.getCookieNames();
		if(allcookies.toArray().length>0){
			String userlogged =(String)allcookies.toArray()[0];
			griglia.R.thereIsANewEvent(griglia.LOGGEDEVENTSREQ,userlogged,new AsyncCallback<ListEvent>() {
				@Override
				public void onSuccess(ListEvent result) {
					if(result!=null){
						griglia.countryGrid.clear();
						RecordList recordList = new RecordList();
						// contaclose � un vettore che cotiene gli id degli eventi chiusi
						griglia.contaClose = new int[result.size()];
						//contatore delgi eventi chiusi
						griglia.scorriContaClose=-1;
						for(Event i:result){
							if((i.getClose()==false) && (new Date().getTime()>i.getEndDatenumber())){
								i.setClose(true);
								griglia.scorriContaClose++;
								griglia.contaClose[griglia.scorriContaClose]=i.getID();
								
							}
							//aggiorno gli eventi scaduti sul client
							Record record = new Record();
							record.setAttribute("nameEvent",i.getName());
							record.setAttribute("locationEvent", i.getLocation());
							record.setAttribute("startEvent", i.getStartEvent());
							record.setAttribute("endEvent", i.getEndEvent());
							record.setAttribute("adminEvent",i.getAdmin());
							record.setAttribute("descriptionEvent", i.getDescription());
							record.setAttribute("closeEvent", i.getClose());
							record.setAttribute("idEvent", i.getID());
							record.setAttribute("invitedNumber", i.getInvitedNumber());
							recordList.add(record);
						}
						if(griglia.scorriContaClose>(-1)){
							//supporto � un vettore che contiene gli eventi chiusi
							int [] supporto= new int [griglia.scorriContaClose+1];
							for(int i = 0 ;i<griglia.contaClose.length;i++){
								supporto[i]=griglia.contaClose[i];
								if(i==griglia.scorriContaClose)break;
							}
							//aggiorna gli eventi chiusi sul database
							griglia.R.chiudiEventi(supporto, new AsyncCallback<Void>() {
								@Override
								public void onSuccess(Void result) {
									
								}
								@Override
								public void onFailure(Throwable caught) {}
							});
						}					
						griglia.countryGrid.setData(recordList);
						griglia.panGrid.clear();
						griglia.panGrid.addMember(griglia.countryGrid);
						griglia.panGrid.draw();
					}
				}
				@Override
				public void onFailure(Throwable caught) {
					//Window.alert(caught.getMessage());
				}
			});
		}
		this.schedule(cooldown);
	}

	public void refreshAnonimus(){
		griglia.R.thereIsANewEvent(griglia.ALLEVENTSREQ,"", new AsyncCallback<ListEvent>() {
			@Override
			public void onSuccess(ListEvent result) {
				if(result!=null){
					griglia.countryGrid.clear();
					RecordList recordList = new RecordList();
					griglia.contaClose = new int[result.size()];
					griglia.scorriContaClose=-1;
					for(Event i:result){
						if((i.getClose()==false) && (new Date().getTime()>i.getEndDatenumber())){
							i.setClose(true);
							griglia.scorriContaClose++;
							griglia.contaClose[griglia.scorriContaClose]=i.getID();
						}
						Record record = new Record();
						record.setAttribute("nameEvent",i.getName());
						record.setAttribute("locationEvent", i.getLocation());
						record.setAttribute("startEvent", i.getStartEvent());
						record.setAttribute("endEvent", i.getEndEvent());
						record.setAttribute("adminEvent",i.getAdmin());
						record.setAttribute("descriptionEvent", i.getDescription());
						record.setAttribute("closeEvent", i.getClose());
						record.setAttribute("idEvent", i.getID());
						record.setAttribute("invitedNumber", i.getInvitedNumber());
						recordList.add(record);
					}
					if(griglia.scorriContaClose>(-1)){
						int [] supporto= new int [griglia.scorriContaClose+1];
						for(int i=0;i<griglia.contaClose.length;i++){
							supporto[i]=griglia.contaClose[i];
							if(i==griglia.scorriContaClose)break;
						}
						griglia.R.chiudiEventi(supporto, new AsyncCallback<Void>() {
							@Override
							public void onSuccess(Void result) {
								
							}
							@Override
							public void onFailure(Throwable caught) {}
						});
					}					
					griglia.countryGrid.setData(recordList);
					griglia.provaaa.clear();
					griglia.provaaa.add(griglia.countryGrid);
				}

			}
			@Override
			public void onFailure(Throwable caught) {
				//Window.alert(caught.getMessage());
			}
		});
		this.schedule(cooldown);
	}

	public void refreshOtherUsers(){
		Collection<String> allcookies = Cookies.getCookieNames();
		if(allcookies.toArray().length>0){
			String userlogged =(String)allcookies.toArray()[0];
			griglia.R.thereIsANewEvent(griglia.OTHEREVENTSREQ,userlogged, new AsyncCallback<ListEvent>() {
				@Override
				public void onSuccess(ListEvent result) {
					if(result!=null){
						griglia.countryGrid.clear();
						RecordList recordList = new RecordList();
						griglia.contaClose = new int[result.size()];
						griglia.scorriContaClose=-1;
						for(Event i:result){
							if((i.getClose()==false) && (new Date().getTime()>i.getEndDatenumber())){
								i.setClose(true);
								griglia.scorriContaClose++;
								griglia.contaClose[griglia.scorriContaClose]=i.getID();
							}
							Record record = new Record();
							record.setAttribute("nameEvent",i.getName());
							record.setAttribute("locationEvent", i.getLocation());
							record.setAttribute("startEvent", i.getStartEvent());
							record.setAttribute("endEvent", i.getEndEvent());
							record.setAttribute("adminEvent",i.getAdmin());
							record.setAttribute("descriptionEvent", i.getDescription());
							record.setAttribute("closeEvent", i.getClose());
							record.setAttribute("idEvent", i.getID());
							record.setAttribute("invitedNumber", i.getInvitedNumber());
							recordList.add(record);
						}          
						if(griglia.scorriContaClose>(-1)){
							int [] supporto= new int [griglia.scorriContaClose+1];
							for(int i = 0 ;i<griglia.contaClose.length;i++){
								supporto[i]=griglia.contaClose[i];
								if(i==griglia.scorriContaClose)break;
							}
							griglia.R.chiudiEventi(supporto, new AsyncCallback<Void>() {
								@Override
								public void onSuccess(Void result) {
									
								}
								@Override
								public void onFailure(Throwable caught) {}
							});
						}
						griglia.countryGrid.setData(recordList);
						griglia.panGrid.clear();
						griglia.panGrid.addMember(griglia.countryGrid);
						griglia.panGrid.draw();
					}
	
				}
	
				@Override
				public void onFailure(Throwable caught) {
					//Window.alert(caught.getMessage());
				}
			});
		}
		this.schedule(cooldown);
	}
}
