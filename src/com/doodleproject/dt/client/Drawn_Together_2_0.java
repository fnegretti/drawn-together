package com.doodleproject.dt.client;

import java.util.Collection;
import com.doodleproject.dt.shared.User;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

//classe pricipale, al primo accesso
public class Drawn_Together_2_0 implements EntryPoint {
	String randomCodes;
	boolean indexpage=true;
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	FocusPanel focus;
	String nomeCookie;
	int tabSelected = 0;
	int contaevento=19;
	int cacca=0;
	Logged1 UserLoggedPage;
	RootPanel root = RootPanel.get();
	public static User utente;
	Grid a = new Grid(greetingService,1);
	VerticalPanel allEventsPanel = new VerticalPanel();
	VerticalPanel primo = new VerticalPanel();
	HorizontalPanel firstLayout = new HorizontalPanel();
	TabPanel loginPanel = new TabPanel();
	Label userNameText = new Label("Username:");
	TextBox userName = new TextBox();
	Label passwordText = new Label("Password:");
	PasswordTextBox password = new PasswordTextBox();
	Button loginButton = new Button("Login");
	HTML registrationLogin = new HTML("<u>Register</u>");
	
	//REGISTRAZIONE
	TextBox usernameRegistration = new TextBox();
	TextBox nameRegistration = new TextBox();
	PasswordTextBox passwordRegistration = new PasswordTextBox();
	PasswordTextBox passwordConfirmRegistration = new PasswordTextBox();
	TextBox mailRegistration = new TextBox();
	Button confirmRegistration = new Button("Register");

	public void onModuleLoad() {
		confirmRegistration.getElement().setId("register");
		loginButton.getElement().setId("login");
		Collection<String> cookies= Cookies.getCookieNames();
		Object[] arrayCookies = cookies.toArray();
		if(arrayCookies.length > 0){
			nomeCookie=(String) cookies.toArray()[0];
			greetingService.controllCookiesinDB(nomeCookie,Cookies.getCookie(nomeCookie), new AsyncCallback<Boolean>() {
				
				@Override
				public void onSuccess(Boolean result) {
					if(result==true){
						indexpage=false;
						System.out.println("successo controllocookie");
						 focus = new FocusPanel();
						 root.add(focus);
						 UserLoggedPage = new Logged1(greetingService,nomeCookie,Drawn_Together_2_0.this);
						 focus.add(UserLoggedPage);
						 focus.addKeyDownHandler(new KeyDownHandler() {
							@Override
							public void onKeyDown(KeyDownEvent event) {
								if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER){
									UserLoggedPage.newEvent.createEventButton.setEnabled(false);
									if(!UserLoggedPage.dataControll())UserLoggedPage.rpcNewEvent();
									else UserLoggedPage.newEvent.createEventButton.setEnabled(true);
								}
							}
						});
					}
					else {
						Cookies.removeCookie(nomeCookie);
						greetingService.deleteCookie(nomeCookie, new AsyncCallback<Boolean>() {
							@Override
							public void onSuccess(Boolean result) {	
							}
							@Override
							public void onFailure(Throwable caught) {
								
							}
						});
						//NESSUN COOKIES O COOKIES NON PRESENTE NEL DB
						focus = new FocusPanel();
						focus.add(firstLayout);
						root.add(focus);
						firstLayout.setSpacing(10);
						loginPanel.add(setLoginTab(),"Login");
						loginPanel.add(createTabRegistration(),"Registration");
						loginPanel.selectTab(0);
						firstLayout.add(loginPanel); 
						loginPanel.addStyleName("tab");
						firstLayout.add(a.gridOfAnonimus());
						firstLayout.setWidth("100%");
						firstLayout.setSpacing(10);
						loginPanel.addSelectionHandler(new SelectionHandler<Integer>() {
							@Override
							public void onSelection(SelectionEvent<Integer> event) {
							  tabSelected= event.getSelectedItem();
							}
						});
						focus.addKeyDownHandler(new KeyDownHandler() {
							@Override
							public void onKeyDown(KeyDownEvent event) {
								if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER){
									if(tabSelected==0) effettuaLogin();
									else effettuaRegistrazione();
								}	
							}
						});
					}	
				}
				@Override
				public void onFailure(Throwable caught) {					
				}
			});
		}
		else{
			//NESSUN COOKIES O COOKIES NON PRESENTE NEL DB
			focus = new FocusPanel();
			focus.add(firstLayout);
			root.add(focus);
			firstLayout.setSpacing(10);
			loginPanel.add(setLoginTab(),"Login");
			loginPanel.add(createTabRegistration(),"Registration");
			loginPanel.selectTab(0);
			firstLayout.add(loginPanel); 
			loginPanel.addStyleName("tab");
			firstLayout.add(a.gridOfAnonimus());
			firstLayout.setWidth("100%");
			firstLayout.setSpacing(10);
			loginPanel.addSelectionHandler(new SelectionHandler<Integer>() {
				@Override
				public void onSelection(SelectionEvent<Integer> event) {
				  tabSelected= event.getSelectedItem();
				}
			});
			focus.addKeyDownHandler(new KeyDownHandler() {	
				@Override
				public void onKeyDown(KeyDownEvent event) {
					if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER){
						if(tabSelected==0) effettuaLogin();
						else effettuaRegistrazione();
					}	
				}
			});
		
		}	
	}
	VerticalPanel createTabRegistration(){
		loginPanel.setAnimationEnabled(true);
		VerticalPanel layoutDialog = new VerticalPanel();
		layoutDialog.setSpacing(15);
		layoutDialog.add(new HTML("Name:"));
		layoutDialog.add(nameRegistration);
		layoutDialog.add(new HTML("Username:"));
		layoutDialog.add(usernameRegistration);
		layoutDialog.add(new HTML("Password:"));
		layoutDialog.add(passwordRegistration);
		layoutDialog.add(new HTML("Confirm Password:"));
		layoutDialog.add(passwordConfirmRegistration);
		layoutDialog.add(new HTML("Mail:"));
		layoutDialog.add(mailRegistration);
		mailRegistration.setText("");
		layoutDialog.add(confirmRegistration);
		confirmRegistration.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				confirmRegistration.setEnabled(false);
			effettuaRegistrazione();	
			}
		});
		return layoutDialog;
	}
	VerticalPanel setLoginTab(){
		loginPanel.setAnimationEnabled(true);
		VerticalPanel loginLayout = new VerticalPanel();
		loginLayout.setSpacing(15);
		loginLayout.add(userNameText);
		loginLayout.add(userName);
		loginLayout.add(passwordText);
		loginLayout.add(password);
		loginLayout.add(loginButton);
		loginButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				loginButton.setEnabled(false);
				effettuaLogin();
			}
		});
		return loginLayout;
	}	
	void effettuaRegistrazione(){
		randomCodes = String.valueOf((int) (Math.random() * (99999 - 1) + 1));
	    while (randomCodes.length() < 5) {
	            randomCodes = "0" + randomCodes;
	        } 
		if(controlDataUser(usernameRegistration.getText(),passwordRegistration.getText(),passwordConfirmRegistration.getText(), mailRegistration.getText(),nameRegistration.getText())==true){
			greetingService.registration(new User(nameRegistration.getText(),
					usernameRegistration.getText(),passwordRegistration.getText(),
					mailRegistration.getText(),randomCodes), new AsyncCallback<String>() {
				@Override
				public void onSuccess(String result) {
					if(result!=null){
						Window.alert("Registrato");
					    Cookies.setCookie(usernameRegistration.getText(), randomCodes, null, "127.0.0.1", "/", false);
						root.clear();
						a.anonimus.cancel();
						Window.Location.reload();
						confirmRegistration.setEnabled(true);
					}else {
						Window.alert("Username in uso!");
						usernameRegistration.setText("");
						passwordRegistration.setText("");
						passwordConfirmRegistration.setText("");
						mailRegistration.setText("");
						nameRegistration.setText("");
						confirmRegistration.setEnabled(true);	
					}
				}
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Errore connessione remota");
				}
			});
		}else confirmRegistration.setEnabled(true);
	}
	boolean controlDataUser(String username, String password, String password2, String email, String name){
		if(username.length()>2 && username.length()<=255){
			if(password.length()>7 && password.length()<=255){
				if(password.contentEquals(password2)){
				if(name.length()>1 && name.length()<=255){
					if(email.length()==0 || (email.length()<=255 && email.contains("@") && email.length()>9))return true;
					else{
						Window.alert("Indirizzo mail errato");
						return false;
					}
				}
				else {Window.alert("nome deve contenere almeno 2 caratteri e non pi� di 255 caratteri");
						return false;
				}
				}
				else{
					Window.alert("le 2 password sono differenti");
					return false;
				}
			}
			else{
				Window.alert(" la password deve contenere almeno 8 caratteri e non pi� di 255 caratteri");
				return false;
			}
		}
		else {
			Window.alert("username deve contenere almeno 3 caratteri e non pi� di 255 caratteri");
			return false;
		}
	}
	void effettuaLogin() {
		try {
			randomCodes = String.valueOf((int) (Math.random() * (99999 - 1) + 1));
		    while (randomCodes.length() < 5) {
		            randomCodes = "0" + randomCodes;
		        } 
				greetingService.login(new User("",userName.getText(),password.getText(),"",randomCodes), new AsyncCallback<String>() {
					@Override
					public void onSuccess(String result) {
						if(result!=null){
							Cookies.setCookie(userName.getText(), randomCodes, null, "127.0.0.1", "/", false);
							root.clear();
							a.anonimus.cancel();
							Window.Location.reload();
							loginButton.setEnabled(true);
						}
						else{
							password.setText("");
							Window.alert("Wrong Username or Password");
							loginButton.setEnabled(true);
						}
					}
					@Override
					public void onFailure(Throwable caught) {
						Window.alert(caught.getMessage());
						return;
					}
				});			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}