package com.doodleproject.dt.client;

import java.util.Collection;
import java.util.Date;
import com.doodleproject.dt.shared.Commento;
import com.doodleproject.dt.shared.ListComment;
import com.doodleproject.dt.shared.Partecipation;
import com.smartgwt.client.widgets.events.ClickHandler; 
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.events.ClickEvent; 
//finestra di dialogo associata agli eventi
public class WindowPartecipation {
	Window windowComment;
	int inviti;
	int contaCommenti = 0;
	HLayout secondaryPanel1;
	VLayout principalPanel;
	VLayout secondaryPanel2;
	HTMLFlow labelEvent;
	DynamicForm newCommentPanel;
	HTMLPane commentAreaBox;
	String chiuso;
	IButton confirmButton, commentButton,deletePartecipationButton;
	TextItem textName;
	TextAreaItem commentArea;
	Canvas canvasMain;
	Boolean status;
	int idEvent;
	GreetingServiceAsync callBack;
	int anonimus;
	AutoRefresh refreshComment;
	Grid cavolo;
	public WindowPartecipation(int invitedNumber,String name,String location, String startDate, String endDate, String admin, String description, String id, GreetingServiceAsync rs,int anonimus,String close,Grid a) {  
		this.inviti =invitedNumber;
		this.cavolo=a;
		this.anonimus=anonimus;
		idEvent = Integer.parseInt(id);
		callBack = rs;
		status = Boolean.parseBoolean(close);
		if(status) chiuso = "Event close";
		else chiuso="Event open";
		//INFO EVENT
		labelEvent = new HTMLFlow();
		labelEvent.setContents(infoEvent(invitedNumber,name, location, startDate, endDate, admin, description,chiuso));
		labelEvent.setAutoHeight();
		labelEvent.setOverflow(Overflow.CLIP_V);
		labelEvent.setShowEdges(false);
		labelEvent.addStyleName("stringstyle");
		//COMMENTS
		commentAreaBox = new HTMLPane();
		commentAreaBox.setContents("Comment");
		commentFormat(callBack);
		commentAreaBox.setWidth(200);
		commentAreaBox.setHeight(150);
		commentAreaBox.setShowEdges(false);
		commentAreaBox.setOverflow(com.smartgwt.client.types.Overflow.AUTO);
		commentButton = new IButton("Comment");
		confirmButton = new IButton("Partecipation");
		//bottone 
		deletePartecipationButton = new IButton("Delete Partecipation");
		//PANNELLO NUOVO COMMENTO
		commentArea = new TextAreaItem();
		commentArea.setRequired(true);
		commentArea.setTitle("Comment");
		commentArea.setWidth(200);
		commentArea.setHeight(150);
		newCommentPanel = new DynamicForm();
		newCommentPanel.setWidth(200);
		newCommentPanel.setFields(new FormItem[] {commentArea});

		secondaryPanel1 = new HLayout();
		secondaryPanel2 = new VLayout();
		principalPanel = new VLayout();
		principalPanel.setWidth(300);
		secondaryPanel2.setMargin(5);
		//controllo se l'utente che interagisce � loggato o anonimo
		if(anonimus==1){
			if(status){
				principalPanel.addMember(labelEvent);
				principalPanel.addMember(commentAreaBox);
			}else{
				principalPanel.addMember(labelEvent);
				principalPanel.addMember(commentAreaBox);
				principalPanel.addMember(confirmButton);
			}
		}
		else {
			if(status){
				principalPanel.addMember(labelEvent);
				principalPanel.addMember(commentAreaBox);
			}else{
				//PANNELLO PRINCIPALE
				secondaryPanel2.addMember(newCommentPanel);
				secondaryPanel2.addMember(commentButton);
				secondaryPanel2.addMember(confirmButton);
				secondaryPanel2.addMember(deletePartecipationButton);
				secondaryPanel1.addMember(commentAreaBox);
				secondaryPanel1.addMember(secondaryPanel2);
				principalPanel.addMember(labelEvent);
				principalPanel.addMember(secondaryPanel1);
			}
		}
		canvasMain = new Canvas();
		windowComment = new Window();
		windowComment.setTitle("EVENT"); 
		windowComment.setCanDragResize(false);  
		windowComment.addItem(principalPanel);
		windowComment.setAutoSize(true);
		windowComment.setHeight(250);
		windowComment.setWidth(550);
		windowComment.centerInPage();
		canvasMain.addChild(windowComment);  
		canvasMain.draw();
		controllPartecipation(); 
		deletePartecipationButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				deletePartecipation();
				deletePartecipationButton.setDisabled(true);
			}
		});
		//bottone partecipazione
		confirmButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				confirmButton.setDisabled(true);
				partecipation();
				}
		});
		//bottone commento
		commentButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if(!(commentArea.getValueAsString()==null)){
					sendComment();
					commentArea.clearValue();
				}
			}
		});
		//refresh all'inserimento di un commento
		refreshComment = new AutoRefresh(callBack,this,'W');
		refreshComment.schedule(10000);
		//chiudi la finestra
		windowComment.addCloseClickHandler(new CloseClickHandler() {
			@Override
			public void onCloseClick(CloseClickEvent event) {
				refreshComment.cancel();
				canvasMain.destroy();
			}
		});
	}
	// invio il commento al server
	public void sendComment(){
		Commento c = new Commento();
		c.setComment(commentArea.getValueAsString());
		c.setEventId(idEvent);
		c.setDate(new Date().getTime());
		if(anonimus==1){}
		else{
			Collection<String> allcookies = Cookies.getCookieNames();
			if(allcookies.toArray().length>0){
			String userlogged =(String)allcookies.toArray()[0];
			c.setUsername(userlogged);
			}
		}
		callBack.insertComment(c, new AsyncCallback<Void>() {
			@Override
			public void onSuccess(Void result) {
				commentAreaBox.setContents("");
				commentFormat(callBack);			
			}
			@Override
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}
		});		  
	}
	//recupera la lista dei commenti dal DB tramite server
	public void commentFormat(GreetingServiceAsync commentCallBack){
		commentCallBack.allCommentForAnEvent(idEvent, new AsyncCallback<ListComment>() {
			String r = "";
			@Override
			public void onSuccess(ListComment result) {
				if(result==null){
					return;
				}
				else{
					for(Commento c : result){
						r += infoComment(c.getUsername(), c.getComment(),c.getDate());
					}
					commentAreaBox.setContents(r);
					return;
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
				return;
			}
		});
		return ;
	}
	// compila il campo precedentemente creato con le informazioni del commento
	public String infoComment(String name, String comment, long date){
		Date a = new Date();
		a.setTime(date);
		return new String(" <span style=\"color: red; font-weight: bold;\"> "+name+" </span> <br/> "
				+ " <span style=\"color: black; font-weight: bold;\"> "+comment+" </span> <br/> "//impostare anche un bacckground se possibile nell'area del commento vero e proprio
				+ " <span style=\"color: green; font-weight: bold;\"> "+a.toString()+" </span> <br/> <br/>");
	}
	//da un autore ad ogni commento
	public String infoComments(String name, String comment, String date){
		return new String("\n"+name+" "+comment+" "+date+"\n");
	}
	// compila il campo precedentemente creato con le informazioni dell' evento
	public String infoEvent(int invitedNumber,String name,String location, String startDate, String endDate, String admin, String description, String chiuso){ 
		return new String("Event: <span style=\"color: green;\">"+name+"</span><br/>"  
				+ "Location: <span style=\"color: green; \">"+location+"</span>"
				+ " Administrator: <span style=\"color: green; \">"+admin+"</span><br/>"
				+ " Start: <span style=\"color: green; \">"+startDate+"</span>"
				+ " End: <span style=\"color: green; \">"+endDate+"</span><br/>"
				+ "Description: <span style=\"color: green; \">"+description+"</span><br/>"
				+ "Status Event: <span style=\"color: green; \">"+chiuso+"</span><br/>"
				+ "Number of guest: <span style=\"color: green; \">"+inviti+"</span>");
	}   
	//comunica al server la partecipazione
	public void partecipation(){
		Partecipation p = new Partecipation();
		p.setEventId(idEvent);
		if(anonimus==1) p.setUsername("Anonimus");
		else {
			Collection<String> allcookies = Cookies.getCookieNames();
			if(allcookies.toArray().length>0){
				String userlogged =(String)allcookies.toArray()[0];
			p.setUsername(userlogged);
			}
		}
		callBack.insertPartecipation(p, new AsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean result) {
				RecordList tmp =  cavolo.countryGrid.getDataAsRecordList();
				String userlogged=new String();
				inviti++;
				tmp.find("idEvent", idEvent).setAttribute("invitedNumber",inviti);
				Collection<String> allcookies = Cookies.getCookieNames();
				if(allcookies.toArray().length>0){
				userlogged =(String)allcookies.toArray()[0];
				if(userlogged == null ){
					cavolo.countryGrid.clear();
					cavolo.countryGrid.setData(tmp);
					cavolo.provaaa.add(cavolo.countryGrid);	
				}
				else{
					cavolo.countryGrid.clear();
					cavolo.countryGrid.setData(tmp);
					cavolo.panGrid.addMember(cavolo.countryGrid);
				}
			}}
			@Override
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}
		});
	}
	//comunica al server l'eliminazione di una partecipazione
	public void deletePartecipation(){
		Partecipation p = new Partecipation();
		p.setEventId(idEvent);
		Collection<String> allcookies = Cookies.getCookieNames();
		if(allcookies.toArray().length>0){
		String userlogged =(String)allcookies.toArray()[0];
		p.setUsername(userlogged);
		callBack.deletePartecipation(p, new AsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean result) {
				RecordList tmp =  cavolo.countryGrid.getDataAsRecordList();
				String userlogged=new String();
				inviti--;
				tmp.find("idEvent", idEvent).setAttribute("invitedNumber",inviti);
				Collection<String> allcookies = Cookies.getCookieNames();
				if(allcookies.toArray().length>0){
					userlogged =(String)allcookies.toArray()[0];
				if(userlogged == null ){
					cavolo.countryGrid.clear();
					cavolo.countryGrid.setData(tmp);
					cavolo.provaaa.add(cavolo.countryGrid);	
				}
				else{
					cavolo.countryGrid.clear();
					cavolo.countryGrid.setData(tmp);
					cavolo.panGrid.addMember(cavolo.countryGrid);
				}
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());

			}
		});
		}
	}
	//controlla che i pulsanti siano sincronizzati a seconda delle partecipazioni
	public void controllDeletePartecipation(){
		if(confirmButton.getDisabled()){
			deletePartecipationButton.setDisabled(false);
		}else{
			deletePartecipationButton.setDisabled(true);
		}	
	}
	public void controllPartecipation(){
		if(anonimus==1);
		else{
			Collection<String> allcookies = Cookies.getCookieNames();
			String userlogged=new String();
			if(allcookies.toArray().length>0){
			userlogged =(String)allcookies.toArray()[0];
			callBack.limitPartecipation(userlogged, idEvent, new AsyncCallback<Boolean>() {
				@Override
				public void onSuccess(Boolean result) {
					if (!result) confirmButton.setDisabled(true);	
					controllDeletePartecipation();
				}
				@Override
				public void onFailure(Throwable caught) {
					System.out.println(caught.getMessage());
				}
			});
			}
		}
	}
}