package com.doodleproject.dt.client;

import java.util.Collection;
import java.util.Date;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.Alignment;  
import com.smartgwt.client.widgets.Canvas;  
import com.smartgwt.client.widgets.IButton;  
import com.smartgwt.client.widgets.events.ClickEvent;  
import com.smartgwt.client.widgets.events.ClickHandler;  
import com.smartgwt.client.widgets.grid.ListGrid;  
import com.smartgwt.client.widgets.grid.ListGridField;  
import com.smartgwt.client.widgets.grid.ListGridRecord;  
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.doodleproject.dt.shared.Event;
import com.doodleproject.dt.shared.ListEvent;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HorizontalPanel;
//ceazione delle tabelle di tutto il progetto
public class Grid extends HorizontalPanel {
	AutoRefresh userLogged,anonimus,otherUsers;
	VLayout panGrid;
	int [] contaClose;
	HorizontalPanel provaaa;
	boolean firstTime = false,firstTime2 = false;
	int scorriContaClose,LOGGEDEVENTSREQ=2,OTHEREVENTSREQ=3,ALLEVENTSREQ=1,typeOfStruct=0;
	WindowPartecipation g;
	GreetingServiceAsync R;
	ListGrid countryGrid;
	public Grid(GreetingServiceAsync RPC, int typeOfStruct){		
		this.typeOfStruct = typeOfStruct; 
		provaaa= new HorizontalPanel();
		this.R = RPC; 
		panGrid= new VLayout();
		countryGrid = new ListGrid() {  
			@Override  
			protected Canvas createRecordComponent(final ListGridRecord record, final Integer colNum) {  
				String fieldName = this.getFieldName(colNum);  
				if (fieldName.equals("infoField")) {  
					IButton info = new IButton();
					HLayout buttonsEvents =new HLayout();
					//bottone info nella griglia
					buttonsEvents.addMember(info);
					//bottoni close e cancel solo per amministratore
					if(Grid.this.typeOfStruct==2){
						final IButton cancel = new IButton();
						final IButton close = new IButton();
						if(Boolean.parseBoolean(record.getAttribute("closeEvent"))){
							close.setDisabled(true);	
						}
						cancel.setTitle("Cancel");
						close.setTitle("Close");
						cancel.setAutoFit(true);
						close.setAutoFit(true);
						buttonsEvents.addMember(close);
						buttonsEvents.addMember(cancel);
						//handler di close e cancella
						cancel.addClickHandler(new ClickHandler() {
							@Override
							public void onClick(ClickEvent event) {
								cancelEvent(Integer.parseInt(record.getAttribute("idEvent")));
								countryGrid.removeData(record);
							}
						});
						close.addClickHandler(new ClickHandler() {
							@Override
							public void onClick(ClickEvent event) {
								closeEvent(Integer.parseInt(record.getAttribute("idEvent")));
								close.setDisabled(true);
							}
						});
					}
					info.setAutoFit(true);
					info.setTitle("Info");
					buttonsEvents.setAutoWidth();
					buttonsEvents.setAutoHeight();
					//handler bottone info
					info.addClickHandler(new ClickHandler() {  
						public void onClick(ClickEvent event) {  
							new WindowPartecipation (record.getAttributeAsInt("invitedNumber"),record.getAttribute("nameEvent"), record.getAttribute("locationEvent"),
									record.getAttribute("startEvent"),record.getAttribute("endEvent"),record.getAttribute("adminEvent"),
									record.getAttribute("descriptionEvent"),record.getAttribute("idEvent"),R,Grid.this.typeOfStruct,
									record.getAttribute("closeEvent"),Grid.this);
						}  
					});  
					return buttonsEvents;  
				} else {  
					return null;  
				}  
			}  
		};
		//informazioni relative agli eventi visibili nelle tabelle e attributi
		countryGrid.setCanSort(false);
		countryGrid.setShowRecordComponents(true);          
		countryGrid.setShowRecordComponentsByCell(true);  
		countryGrid.setCanRemoveRecords(false);  
		countryGrid.setAutoFitWidth("nameEvent", true);
		countryGrid.setAutoFitWidth("locationEvent", true);
		countryGrid.setAutoFitWidth("startEvent", true);
		countryGrid.setAutoFitWidth("endEvent", true);
		countryGrid.setAutoFitWidth("adminEvent", true);
		countryGrid.setAutoFitWidth("infoField", true);
		countryGrid.setWidth(800);  
		countryGrid.setHeight(300);  
		countryGrid.setShowAllRecords(true);
		//compilazione campi
		ListGridField nameField = new ListGridField("nameEvent", "Event"); 
		ListGridField locationField = new ListGridField("locationEvent", "Location");  
		ListGridField startField = new ListGridField("startEvent", "Start");  
		ListGridField endField = new ListGridField("endEvent", "End"); 
		ListGridField adminField = new ListGridField("adminEvent", "Administrator");  
		ListGridField infoField = new ListGridField("infoField", "Partecipation");  
		infoField.setAlign(Alignment.CENTER);
		countryGrid.setFields(nameField, locationField, startField, endField, adminField, infoField);  
		countryGrid.setCanResizeFields(true);  
	}
	// tabella per gli utenti loggati
	public VLayout gridofUserLogged(){
		//recupera i cookie
		Collection<String> allcookies = Cookies.getCookieNames();
		if(allcookies.toArray().length>0){
			String userlogged =(String)allcookies.toArray()[0];
			//richiesta al server della lista eventi
			R.allEventsUser(userlogged, new AsyncCallback<ListEvent>() {
				@Override
				public void onSuccess(ListEvent result) {
					if(result!=null){
						RecordList recordList = new RecordList();
						scorriContaClose =-1;
						contaClose = new int[result.size()];
						//riempie la griglia
						for(Event i: result)
						{
							if((i.getClose()==false) && (new Date().getTime()>i.getEndDatenumber())){
								i.setClose(true);
								scorriContaClose++;
								contaClose[scorriContaClose]=i.getID();
							}
							Record record = new Record();
							record.setAttribute("nameEvent",i.getName());
							record.setAttribute("locationEvent", i.getLocation());
							record.setAttribute("startEvent", i.getStartEvent());
							record.setAttribute("endEvent", i.getEndEvent());
							record.setAttribute("adminEvent",i.getAdmin());
							record.setAttribute("descriptionEvent", i.getDescription());
							record.setAttribute("closeEvent", i.getClose());
							record.setAttribute("idEvent", i.getID());
							record.setAttribute("invitedNumber", i.getInvitedNumber());
							recordList.add(record);
						} 
						if(scorriContaClose>(-1)){
							int [] supporto= new int [scorriContaClose+1];
							for(int i = 0;i<contaClose.length;i++){
								supporto[i]=contaClose[i];
								if(i==(scorriContaClose))break;
							}
							R.chiudiEventi(supporto, new AsyncCallback<Void>() {
								@Override
								public void onSuccess(Void result) {}
								@Override
								public void onFailure(Throwable caught) {}
							});
						}
						countryGrid.setData(recordList);
					}
				}
				@Override
				public void onFailure(Throwable caught) {
					System.out.println(caught.getMessage());
				}
			});
		}
		//aggiunge la tabella e si prepara al refresh
		panGrid.addMember(countryGrid);
		userLogged = new AutoRefresh(this,'L');
		userLogged.schedule(userLogged.cooldown);
		return panGrid;
	}
	//tabella degli utenti anonimi
	public HorizontalPanel gridOfAnonimus(){
		R.allEvents(new AsyncCallback<ListEvent>() {
			@Override
			public void onSuccess(ListEvent result) {
				if(result!=null){
					RecordList recordList = new RecordList();
					scorriContaClose =-1;
					contaClose = new int[result.size()];
					for(Event i: result)
					{
						if((i.getClose()==false) && (new Date().getTime()>i.getEndDatenumber())){
							i.setClose(true);
							scorriContaClose++;
							contaClose[scorriContaClose]=i.getID();
						}	
						Record record = new Record();
						record.setAttribute("nameEvent",i.getName());
						record.setAttribute("locationEvent", i.getLocation());
						record.setAttribute("startEvent", i.getStartEvent());
						record.setAttribute("endEvent", i.getEndEvent());
						record.setAttribute("adminEvent",i.getAdmin());
						record.setAttribute("descriptionEvent", i.getDescription());
						record.setAttribute("closeEvent", i.getClose());
						record.setAttribute("idEvent", i.getID());
						record.setAttribute("invitedNumber", i.getInvitedNumber());
						recordList.add(record);
					} 
					if(scorriContaClose>(-1)){
						int [] supporto= new int [scorriContaClose+1];
						for(int i = 0;i<contaClose.length;i++){
							supporto[i]=contaClose[i];
							if(i==(scorriContaClose))break;	
						}
						R.chiudiEventi(supporto, new AsyncCallback<Void>() {
							@Override
							public void onSuccess(Void result) {}
							@Override
							public void onFailure(Throwable caught) {}
						});
					}
					countryGrid.setData(recordList);
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}
		});
		provaaa.add(countryGrid);
		anonimus = new AutoRefresh(this,'A');
		anonimus.schedule(anonimus.cooldown);
		return provaaa;               
	}
	//tabella degli eventi di altri utenti (nello stato Loggato)
	public VLayout gridOfOtherUsers(){
		Collection<String> allcookies = Cookies.getCookieNames();
		if(allcookies.toArray().length>0){
			String userlogged =(String)allcookies.toArray()[0];
			R.allEventsOfOtherUsers(userlogged, new AsyncCallback<ListEvent>() {
				@Override
				public void onSuccess(ListEvent result) {
					if(result!=null){
						RecordList recordList = new RecordList();
						scorriContaClose =-1;
						contaClose = new int[result.size()];
						for(Event i: result)
						{
							if((i.getClose()==false) && (new Date().getTime()>i.getEndDatenumber())){
								i.setClose(true);
								scorriContaClose++;
								contaClose[scorriContaClose]=i.getID();
							}
							Record record = new Record();
							record.setAttribute("nameEvent",i.getName());
							record.setAttribute("locationEvent", i.getLocation());
							record.setAttribute("startEvent", i.getStartEvent());
							record.setAttribute("endEvent", i.getEndEvent());
							record.setAttribute("adminEvent",i.getAdmin());
							record.setAttribute("descriptionEvent", i.getDescription());
							record.setAttribute("closeEvent", i.getClose());
							record.setAttribute("idEvent", i.getID());
							record.setAttribute("invitedNumber", i.getInvitedNumber());
							recordList.add(record);
						} 
						if(scorriContaClose>(-1)){
							int [] supporto= new int [scorriContaClose+1];
							for(int i = 0;i<contaClose.length;i++){
								supporto[i]=contaClose[i];
								if(i==(scorriContaClose))break;
							}
							R.chiudiEventi(supporto, new AsyncCallback<Void>() {
								@Override
								public void onSuccess(Void result) {}
								@Override
								public void onFailure(Throwable caught) {}
							});
						}
						countryGrid.setData(recordList);
					}
				}
				@Override
				public void onFailure(Throwable caught) {
					System.out.println(caught.getMessage());
				}
			});
		}
		panGrid.addMember(countryGrid);
		otherUsers = new AutoRefresh(this, 'O');
		otherUsers.schedule(otherUsers.cooldown);
		return panGrid;
	}
	//se l'amministratore vuole chiudere un evento chiamata al server
	public void closeEvent(int idEvent){
		R.closeEvent(idEvent, new AsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean result) {
				if(result)Window.alert("Event closed");
				else Window.alert("Procedure Error");
				Window.Location.reload();
			}
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());				
			}
		});

	}
	//se l'amministratore vuole cancellare un evento chiamata al server
	public void cancelEvent(int idEvent){
		R.cancelEvent(idEvent, new AsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean result) {
				if(result)Window.alert("Event canceled");
				else Window.alert("Procedure Error");
			}
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());				
			}
		});
	}
}