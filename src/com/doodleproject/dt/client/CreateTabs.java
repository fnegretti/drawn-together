package com.doodleproject.dt.client;

import com.smartgwt.client.types.Side;  
import com.smartgwt.client.widgets.layout.VLayout;  
import com.smartgwt.client.widgets.tab.Tab;  
import com.smartgwt.client.widgets.tab.TabSet;

public class CreateTabs extends VLayout{
	
	TabSet topTabSet = new TabSet(); 
	Grid myEvents,otherEvents;
	// crea la struttura delle tabs degli utenti loggati
	public CreateTabs(GreetingServiceAsync RPC) {
		
		myEvents = new Grid(RPC,2);
		otherEvents= new Grid(RPC,3);
		topTabSet.setTabBarPosition(Side.TOP);  
	    topTabSet.setWidth(800);  
	    topTabSet.setHeight(520);  

	    Tab tTab1 = new Tab("My events", "");    //aggiungere icone
	    tTab1.setPane(myEvents.gridofUserLogged());
	    Tab tTab2 = new Tab("Other events", "");    //aggiungere icone
	    tTab2.setPane(otherEvents.gridOfOtherUsers());
	    topTabSet.addTab(tTab1);  
	    topTabSet.addTab(tTab2);
	    topTabSet.addStyleName("tab");
        this.addMember(topTabSet);   
        this.setHeight("*");
        this.setWidth("*");
        this.draw();
	}
}
