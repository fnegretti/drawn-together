package com.doodleproject.dt.client;

import java.util.Collection;
import java.util.Date;
import com.doodleproject.dt.shared.Event;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.widgets.HTMLFlow;
 
//schermata visualizzata dopo il login
public class Logged1 extends HorizontalPanel {
	Button logout;
	TabPanel body;
	Drawn_Together_2_0 indexPage;
	public CreateTabs vLayout;
	public NewEventWindowLogged newEvent;
	public Logged1(GreetingServiceAsync RPC, String admin, Drawn_Together_2_0 indexPage){
		this.indexPage = indexPage;
		VerticalPanel vp = new VerticalPanel();
		HorizontalPanel info = new HorizontalPanel();
		HTMLFlow userinfo = new HTMLFlow();
		String cookieinfo =(String) Cookies.getCookieNames().toArray()[0];
		String welcometext = "Welcome "+cookieinfo;
		logout = new Button("Logout");
		newEvent = new NewEventWindowLogged(RPC,admin,this.indexPage);
        vLayout = new CreateTabs(RPC);
		body = new TabPanel();
		logout.getElement().setId("logout");
		userinfo.setContents(welcometext);
		userinfo.addStyleName("stringinfo");
		body.add(newEvent.createWindow(), "NEW EVENT");
		body.selectTab(0);
		info.add(userinfo);
		info.add(logout);
		vp.setSpacing(10);
		vp.add(info);
		vp.add(body);
		this.add(vp);
        this.add(vLayout);
        this.setWidth("100%");
        body.addStyleName("tab");
    	vp.setCellVerticalAlignment(logout, HasVerticalAlignment.ALIGN_MIDDLE);
		vp.setCellHorizontalAlignment(logout, HasHorizontalAlignment.ALIGN_CENTER);
		//bottone logout
		logout.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				vLayout.myEvents.userLogged.cancel();
				vLayout.otherEvents.otherUsers.cancel();
				Collection<String> cookies = Cookies.getCookieNames();
				String nomeCookie=(String) cookies.toArray()[0];
				Cookies.removeCookie(nomeCookie);
				Logged1.this.indexPage.root.clear();
				Window.Location.reload();
			}
		});
		//bottone nuovo evento
		newEvent.createEventButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				newEvent.createEventButton.setEnabled(false);
				if(!dataControll())rpcNewEvent();
				else newEvent.createEventButton.setEnabled(true);
			}
		});	
	}
	// campi da compilare per nuovo evento
	public boolean dataControll(){
		newEvent.startDate = new Date();
		long startTimeMS,endTimeMS;
		int startHour,endHour,startMin,endMin;
		String startString,endString;
		//controlli che siano valori corretti
		if( newEvent.eventName.getText().length()<3 || newEvent.eventName.getText().contentEquals("Name:")||newEvent.eventName.getText().length()>255||newEvent.location.getText().length()<3 ||newEvent.location.getText().length()>255||newEvent.location.getText().contentEquals("Location:")){
			Window.alert("Name e Location devono essere modificati e contenere almeno 3 caratteri e non pi� di 255");
			return true;
		}
		if(newEvent.description.getText().contentEquals("Description:")||newEvent.description.getText().length()>500){
			Window.alert("settare il campo description con un massimo di 500 caratteri");
			return true;
		}
		if(newEvent.startEventLabel.getText().length()==0 || newEvent.endEventLabel.getText().length()==0){
			Window.alert("selezionare data di inizio e di fine evento");
			return true;
		}
		startString = newEvent.startEventHour.getText();
		endString = newEvent.endEventHour.getText();
		if (startString.length()!=5 || endString.length()!=5){
			Window.alert("devono esserci esattamente 5 caratteri, formato orario: HH:MM");
			return true;
		}
		if(!(startString.contains(":")||endString.contains(":"))){
			Window.alert(" 11 errore nell'orario, formato HH:MM");
		}
		try{
			startHour= Integer.parseInt(startString.substring(0,2));
			endHour = Integer.parseInt(endString.substring(0,2));
			startMin = Integer.parseInt(startString.substring(3,5));
			endMin = Integer.parseInt(endString.substring(3,5));
			if ((startHour>23 || endHour>23) || (startMin>59 || endMin>59)){	
				Window.alert("ora o minuti con valori non validi");
				return true;
			}
			//conversione dai MS
			startTimeMS = newEvent.start + (startHour*1000*60*60)+(startMin*1000*60);
			endTimeMS = newEvent.end + (endHour*1000*60*60)+(endMin*1000*60);
			if(endTimeMS-startTimeMS<=0 ){
				Window.alert("data di fine evento minore della data di inizio evento");
				return true;
			}
			newEvent.startDate.setTime(startTimeMS);
			newEvent.endDate.setTime(endTimeMS);
		}catch(NumberFormatException e){
			Window.alert("errore nell'orario, formato orario :  HH:MM ");
			return true;
		}
		return false;
	}
	//recupera i campi inseriti se sono corretti e li passa al server
	public void rpcNewEvent(){
		Event e = new Event();
		e.setName(newEvent.eventName.getText());
		e.setLocation(newEvent.location.getText());
		e.setDescription(newEvent.description.getText());
		e.setStartEvent(newEvent.startEventLabel.getText()+" "+newEvent.startEventHour.getText());
		e.setEndEvent(newEvent.endEventLabel.getText()+" "+newEvent.endEventHour.getText());
		e.setAdmin(newEvent.admin);
		e.setStartDatenumber(newEvent.startDate.getTime());
		e.setEndDatenumber(newEvent.endDate.getTime());
		newEvent.RPC.insertEvent(e, new AsyncCallback<Void>() {
			@Override
			public void onSuccess(Void result) {
				newEvent.createEventButton.setEnabled(true);
				Window.alert("EVENT CREATED");
				newEvent.eventName.setText("");
				newEvent.description.setText("");
				newEvent.location.setText("");
				newEvent.startEventHour.setText("");
				newEvent.endEventHour.setText("");
				//REFRESH
				vLayout.myEvents.clear();
				vLayout.myEvents.gridofUserLogged();
			}
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
			}
		});
	}
}