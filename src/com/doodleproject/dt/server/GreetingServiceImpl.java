package com.doodleproject.dt.server;

import java.io.File;

import org.mindrot.BCrypt;
import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.DBMaker;


import com.doodleproject.dt.client.GreetingService;
import com.doodleproject.dt.shared.Commento;
import com.doodleproject.dt.shared.Event;
import com.doodleproject.dt.shared.ListComment;
import com.doodleproject.dt.shared.ListEvent;
import com.doodleproject.dt.shared.Partecipation;
import com.doodleproject.dt.shared.User;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

// classe server-side
@SuppressWarnings("serial")
// creazione del DB
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService {
	public static DB db=null;
	public static void openDB(){ 
	db = DBMaker.newFileDB(new File("testdb"))
    .closeOnJvmShutdown()
    // .encryptionEnable("password")
    .make();
	}
	

public GreetingServiceImpl(){
//	   openDB();
//	   db.close();
}
//registrazione di un nuovo utente	
public String registration(User user) throws IllegalArgumentException {
	openDB();
	int count;
	String result=user.getRandomCookieValue();
	BTreeMap<Integer, User> users = db.getTreeMap("users");
	String pwdCrypt = BCrypt.hashpw(user.getPassword(),BCrypt.gensalt(10));
	String pwdnocrypt = user.getPassword();
	user.setPassword(pwdCrypt);
	// aggiunge un nuovo user in coda alla mappa se non esiste gi�
	try{
		count=users.lastKey();
		for(Integer i:users.keySet()){
			User tmp = users.get(i);
			if(tmp.getUsername().contentEquals(user.getUsername())){
				result=null;
				break;
			}
			
		}
		//USERNAME NON TROVATO QUINDI LO AGGIUNGO
		if(result!=null)users.put(count+1, user);
	}
	catch(Exception e){
		//TABELLA DEGLI UTENTI VUOTA
		users.put(0,user);
	}
	if(result!=null){
		try{
			//salva sul database la coppia user-token
			BTreeMap<String, String> cookie = db.getTreeMap("cookie");
			cookie.put(user.getUsername(), user.getRandomCookieValue());
			db.commit();
			System.out.println("REGISTRATION COMMIT");
		}catch(Exception e){
			e.printStackTrace();
			result=null;
		}
	}

	db.close();
	user.setPassword(pwdnocrypt);
	return result;
}
// effettua login
public String login(User user){
	String username= user.getUsername();
	String pwd = user.getPassword();
	String result=null;
	User Utmp = new User();
	openDB();
	BTreeMap<Integer, User> users = db.getTreeMap("users");
	try{
		//scorri la mappa degli utenti e cerca se contiene lo username
		users.lastKey();
		for(Integer i:users.keySet()){
			Utmp = users.get(i);
			//controllo user e password
			System.out.println("DBusername:"+Utmp.getUsername()+"=="+user.getUsername()+" DBuserpwd:"+BCrypt.checkpw(user.getPassword(),Utmp.getPassword())+"pwd:"+user.getPassword());
			if(Utmp.getUsername().contentEquals(username) && BCrypt.checkpw(pwd, Utmp.getPassword())){
				System.out.println("pwd:"+pwd+" passwordDB:"+Utmp.getPassword());
				//assegna il token al cookie se si logga
				result=Utmp.getRandomCookieValue();
				break;
			}
		}
	}catch(Exception e){result=null;}//DB VUOTO	
	if(result!=null){
		//salva la coppia user-token
		try{
			BTreeMap<String, String> cookie = db.getTreeMap("cookie");
			cookie.put(user.getUsername(), user.getRandomCookieValue());
			db.commit();
		}catch(Exception e){
			result=null;
		}
	}
	db.close();
	return result;
}

// inserimento nuovo evento nel DB	
public void insertEvent(Event e){
	int i;
	openDB();
	BTreeMap<Integer, Event> event = db.getTreeMap("event");
	// verifico il valore di lastkey, per assegnare indice di partenza e vedere che la mappa non sia vuota 
	try {
		//lastkey diverso da null i prende il suo valore
		i= event.lastKey();
	}
	catch(Exception er ){
		//se no prende -1 (con i++ va a 0)
		i=-1;
	}
	i++;
	e.setID(i);
	event.put(i,e);// inserisco evento
	db.commit();
	db.close();
}
//inserimento partecipazione a uno specifico evento
public boolean insertPartecipation(Partecipation p) {
	boolean result = false;	
	int invitedNumber=0,conta;
	int idEvento = p.getEventId();
	Event Etemp = new Event();
	Partecipation Ptemp= new Partecipation();
	openDB();
	BTreeMap<Integer, Partecipation> sondaggio = db.getTreeMap("sondaggio");
	BTreeMap<Integer, Event> event = db.getTreeMap("event");
	try {
		 conta= sondaggio.lastKey();
		 result=true;
	}catch(Exception er ){
		conta=-1;
	}
	conta++;
	sondaggio.put(conta, p);
	// inserimento partecipazione al rispettivo sondaggio
	for(Integer i:sondaggio.keySet()){
		Ptemp = sondaggio.get(i);
		if(Ptemp.getEventId()==p.getEventId())invitedNumber++;
	}
	// salvo nella tabella degli eventi il numero di partecipanti aggiornato
	for(Integer i:event.keySet()){
		Etemp = event.get(i);
		if(Etemp.getID() == idEvento){
			Etemp.setInvitedNumber(invitedNumber);
			event.put(i, Etemp);
			result=true;
			break;
		}
	}
	db.commit();
	db.close();	
	return result;
}
// serve per impedire ad uno stesso utente di mettere pi� partecipazioni ad uno stesso sondaggio
public boolean limitPartecipation(String username, int idEvento) {
	boolean result = true;
	Partecipation Stemp = new Partecipation();
	openDB();
	BTreeMap<Integer, Partecipation> sondaggio = db.getTreeMap("sondaggio");
	try{
		sondaggio.lastKey();
		// controllo se username ha gi� messo partecipazione allo specifico evento
		for(Integer i:sondaggio.keySet()){
			Stemp = sondaggio.get(i);
			String Utmp = Stemp.getUsername();
			if((Stemp.getEventId() == idEvento)&&(Utmp.contentEquals(username))){
				result=false;
				break;
			}
		}
	}catch(Exception e){}//DB VUOT
	db.close();
	return result;
}
// inserire un commento di un utente a un evento
public void insertComment(Commento e) {
		int i;
		openDB();
		BTreeMap<Integer, Commento> commenti = db.getTreeMap("commenti");
		e.setDate(getCurrentDate());
		try{
			i= commenti.lastKey();
		}
		catch(Exception ex){ //DB VUOTO
			i=-1;
		}
		i++;
		commenti.put(i, e);// inserisco commento in DB
		db.commit();
		db.close();
}
//lista di tutti i commenti per un preciso evento
public ListComment allCommentForAnEvent(int idEvent) {
	Commento Ctemp;
	ListComment allComment;
	openDB();
	BTreeMap<Integer, Commento> commenti = db.getTreeMap("commenti");
	try{
		commenti.lastKey();
		allComment = new ListComment();
		//crea la lista dei commenti per lo specifico evento chiamato
		for(Integer i:commenti.keySet()){
			Ctemp = commenti.get(i);
			if(Ctemp.getEventId()==idEvent)allComment.add(Ctemp);
		}
	}catch (Exception ex){
		allComment=null;
	}
	db.close();	
	return allComment;
}
//lista di tutti gli eventi	
public ListEvent allEvents() {
	openDB();
	BTreeMap<Integer, Event> event = db.getTreeMap("event");
	ListEvent allEvent;
	try{
		event.lastKey();
		allEvent= new ListEvent();
		for(Integer i:event.keySet())
		     allEvent.add(event.get(i));
	}
	catch(Exception e){
		allEvent=null;
	}		
	db.close();
	return allEvent;
}
// recupero la data di oggi da impostare nel calendario
private static Long getCurrentDate() {
	java.util.Date today = new java.util.Date();
	return today.getTime();
}
//settare un evento a chiuso	
public boolean closeEvent(int idEvent) {
	Event Etemp;
	//inizialmente aperto
	boolean result=false;
	openDB();
	BTreeMap<Integer, Event> event = db.getTreeMap("event");
	try{
		event.lastKey();
		for(Integer i:event.keySet()){
			Etemp=event.get(i);
			//controllo che sia aperto e che gli ID corrispondano
			if((Etemp.getID()==idEvent) && (Etemp.getClose()==false)){
				Etemp.setClose(true);
				event.put(i, Etemp);
				db.commit();
				result=true;
				break;
			}
		}
	} catch(Exception e){}
	db.close();
	return result;
}
//cancellazione di un evento dal DB
public boolean cancelEvent(int idEvent) {
		boolean result = false;
		Event Etemp;
		//dor�cancllare anche commenti e partecipazioni associati
		Partecipation Stemp;
		Commento Ctemp;
		openDB();
		BTreeMap<Integer, Event> event = db.getTreeMap("event");
		BTreeMap<Integer, Partecipation> sondaggio = db.getTreeMap("sondaggio");
		BTreeMap<Integer, Commento> commenti = db.getTreeMap("commenti");
		try{
			event.lastKey();
			for(Integer i:event.keySet()){
				Etemp=event.get(i);
				if(Etemp.getID()==idEvent){
					event.remove(i);//riuovo l'evento
					result = true;
					break;
				}}
			if(result = true){
				for(Integer i:sondaggio.keySet()){
					Stemp= sondaggio.get(i);
					if(Stemp.getEventId()==idEvent)
						sondaggio.remove(i);//rimuovo le partecipazioni
				}
				for(Integer i:commenti.keySet()){
					Ctemp=commenti.get(i);
					if(Ctemp.getEventId()==idEvent)
						commenti.remove(i);//rimuovo i commenti
				}
				db.commit();
			}
		}catch(Exception e){}
		db.close();
		return result;
	}
//lista degli eventi associati ad un utente (amministratore)
public ListEvent allEventsUser(String user) {
		openDB();
		BTreeMap<Integer, Event> event = db.getTreeMap("event");
		ListEvent allEvent;
		Event Etemp;
		try{
			event.lastKey();
			allEvent = new ListEvent();
			for(Integer i:event.keySet()){
				Etemp=event.get(i);
				//se user e amministratore corrispondono aggiungo l'evento alla lista
				if(Etemp.getAdmin().contentEquals(user))
					allEvent.add( event.get(i) );
			}
		} 
		catch (Exception x){
			allEvent = null;
		} 
		db.close();
		return allEvent;
	}
//lista degli eventi creati da alri utenti
public ListEvent allEventsOfOtherUsers(String user) {
		openDB();
		BTreeMap<Integer, Event> event = db.getTreeMap("event");
		ListEvent allEvent;
		Event Etemp;
		try{
			event.lastKey();
			allEvent= new ListEvent();
			for(Integer i:event.keySet()){
				Etemp=event.get(i);
				//compilo la lista se user e admin non corrispondono
				if(!(Etemp.getAdmin().contentEquals(user)))
					allEvent.add( event.get(i) );
				}
		}catch(Exception e){
			allEvent=null;
		}
		db.close();
		return allEvent;
	}
	// cerca nelle specifiche tabelle se esiste un nuovo evento creato,per refresh
	public ListEvent thereIsANewEvent(int typeOfSearch, String admin) {
		openDB();
		BTreeMap<Integer, Event> event = db.getTreeMap("event");
		ListEvent allEvent;
		Event Etemp;
		try{
			event.lastKey();
			allEvent = new ListEvent();
			//id settato dinamicamente a lato client (Grid.java)
			switch (typeOfSearch) {
			case 1:
				// tutti gli eventi
				for(Integer i:event.keySet())
					allEvent.add(event.get(i));	
				break;
			case 2:
				// eventi dell'utente loggato
				for(Integer i:event.keySet()){
					Etemp = event.get(i);
					if(Etemp.getAdmin().contentEquals(admin))
						allEvent.add(event.get(i));
				}
				break;
			case 3:
				// eventi di tutti gli altri utenti
				for(Integer i:event.keySet()){
					Etemp = event.get(i);
					if(!(Etemp.getAdmin().contentEquals(admin)))
							allEvent.add(event.get(i));
				}
				break;
			}
		}catch(Exception x){
			allEvent=null;
		}
		db.close();
	  return allEvent;
	}
// permette di rimuovere una partecipazione
	@Override
	public boolean deletePartecipation(Partecipation p) {
		boolean result = false;
		int idEvento = p.getEventId();
		String name= p.getUsername();
		Event Etemp;
		Partecipation Ptemp;
		openDB();
		BTreeMap<Integer, Partecipation> sondaggio = db.getTreeMap("sondaggio");
		BTreeMap<Integer, Event> event = db.getTreeMap("event");
		try{
		 sondaggio.lastKey();	
			for(Integer i:sondaggio.keySet()){
				Ptemp = sondaggio.get(i);
				//controlla che useername loggato sia lo stesso che ha messo la partecipazione
				if((Ptemp.getEventId()==idEvento)&&(Ptemp.getUsername().contentEquals(name))){
					sondaggio.remove(i);//rimuove partecipazione dal DB
					result = true;
					break;
				}
			}
			if(result=true){
				for(Integer i:event.keySet()){
					Etemp = event.get(i);
					//aggiorno il numero di partecipanti
					if(Etemp.getID() == idEvento){
						int invitedNumber=Etemp.getInvitedNumber()-1;
						Etemp.setInvitedNumber(invitedNumber);
						event.put(i, Etemp);
						db.commit();
						break;
					}
				}
			}
		}		
		catch(Exception e){}		
		return result;
	}

	@Override
	public boolean controllCookiesinDB(String session,String value ) {
		boolean result=true;
		openDB();
		BTreeMap<String, String> cookie = db.getTreeMap("cookie");
		try{
			cookie.lastKey();
			System.out.println("DBcookiename:"+session+"cookievalue:"+cookie.get(session)+"cookievalueparametro:"+value);
			if(cookie.get(session).contentEquals(value)!=true)result=false;
		}catch(Exception e){
			System.out.println("DBcookie non c'� cookie");
			result=false;
			}//DB VUOTO
		db.close();
		System.out.println(""+result);
		return result;
	}
	//usato nel test per cancellare il contenuto del DB quando ci serve
	public void cancellaDBContent(){
		openDB();
		db.getTreeMap("users").clear();
		db.getTreeMap("sondaggio").clear();
		db.getTreeMap("commenti").clear();
		db.getTreeMap("event").clear();
		db.getTreeMap("cookie").clear();
		db.commit();
		db.close();
	}
	//chiusura degli eventi chiamata dall'autorefresh
	@Override
	public void chiudiEventi(int[] vect) {
		openDB();
		BTreeMap<Integer, Event> event = db.getTreeMap("event");
		try{
			event.lastKey();
			for(int i=0;i<vect.length;i++){
				Event tmp=event.get(vect[i]);
				tmp.setClose(true);
				event.put(vect[i], tmp);
			}
			
			db.commit();
		}catch(Exception e){}
		db.close();
	}
	@Override
	public boolean deleteCookie(String nameCookie) {
		boolean result=true;
		openDB();
		BTreeMap<String, String> cookie = db.getTreeMap("cookie");
		try{
			cookie.lastKey();
			cookie.remove(nameCookie);
			db.commit();
			
		}catch(Exception e){result=false;}//DB VUOTO
		db.close();
		return result;
	}
}