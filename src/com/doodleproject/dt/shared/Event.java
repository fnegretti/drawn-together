package com.doodleproject.dt.shared;

import java.io.Serializable;
//definizione dell'entit� evento e relativi campi
public class Event implements Serializable{

	private static final long serialVersionUID = 572208713286063322L;
	private String name;
	private String location;
	private String description;
	private String startEvent;
	private String endEvent;
	private String admin;
	private long startDatenumber;
	private long endDatenumber;
	private int invitedNumber;
	private int ID;
	private boolean close;


	public Event (){
		this.setName("empty");
		this.setLocation("empty");
		this.setDescription("empty");
		this.setAdmin("empty");
		this.setStartEvent("empty");
		this.setEndEvent("empty");
		this.setInvitedNumber(0);
		this.setID(0);
		this.setClose(false);
		this.setStartDatenumber(0);

	}
	public Event (String name,String location,String description,String admin, String start, String end){
		this.setName(name);
		this.setLocation(location);
		this.setDescription(description);
		this.setAdmin(admin);
		this.setStartEvent(start);
		this.setEndEvent(end);
		this.setInvitedNumber(0);
		this.setID(0);
		this.setClose(false);
		this.setStartDatenumber(0);

	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAdmin() {
		return admin;
	}
	public void setAdmin(String admin) {
		this.admin = admin;
	}
	public String getStartEvent() {
		return startEvent;
	}
	public void setStartEvent(String startEvent) {
		this.startEvent = startEvent;
	}
	public String getEndEvent() {
		return endEvent;
	}
	public void setEndEvent(String endEvent) {
		this.endEvent = endEvent;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public int getInvitedNumber() {
		return invitedNumber;
	}
	public void setInvitedNumber(int invitedNumber) {
		this.invitedNumber = invitedNumber;
	}
	public boolean getClose() {
		return close;
	}
	public void setClose(boolean close) {
		this.close = close;
	}
	public long getStartDatenumber() {
		return startDatenumber;
	}
	public void setStartDatenumber(long startDatenumber) {
		this.startDatenumber = startDatenumber;
	}
	public long getEndDatenumber() {
		return endDatenumber;
	}
	public void setEndDatenumber(long endDatenumber) {
		this.endDatenumber = endDatenumber;
	}

}