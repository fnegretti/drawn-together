package com.doodleproject.dt.shared;

import java.io.Serializable;

// definizione dell'entit� commento e relativi campi 
public class Commento implements Serializable{
	
	private static final long serialVersionUID = -2839318176465994021L;
	private int eventId;
	private String comment;
	private String username;
	private long date;
	
	public Commento(){
		setComment(null);
		setEventId(0);
		setUsername(null);
		setDate(0);

	}
	public Commento(String comment,int id,String username){
		setComment(comment);
		setEventId(0);
		setUsername(username);
		setDate(0);

	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}
}
