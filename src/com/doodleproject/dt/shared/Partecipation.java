package com.doodleproject.dt.shared;

import java.io.Serializable;
//definizione dell'entit� partecipazione e relativi campi
public class Partecipation implements Serializable{

	private static final long serialVersionUID = 9150424878469528397L;
	private String username;
	private int eventId;

	public Partecipation(){
		setEventId(0);
		setUsername("empty");
	}
	public Partecipation(int id, String username){
		setEventId(id);
		setUsername(username);
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
